# README #

This is a web application for an online store that caters cakes and treats. It is developed using Express web application framework and data is stored into MongoDB. Basically all the customer orders are saved and can be viewed. On successful checkout the receipt is printed at the end
and can be viewed by the customer.

### How do I get it set up? ###
 
Steps to install, build and use this project.
 
1. Install github in your machine.
2. Run git clone command to clone the repository to local.
3. Install node.js in your machine.
4. Open the project in Visual Studio Code.
5. Open terminal window.
6. Run command npm install to download missing libraries.
7. Run command node index.js to run the application.
8. Open browser and enter the address https://localhost:8088 to view the website

### License ###

This projected is licensed under MIT. 
I opted to license my project under MIT since its simple and permissive. 
Basically, you can do whatever you want with software licensed under the MIT license —  as long as you add a copy of the original MIT license and copyright notice to it. The MIT License is very simple, short and to the point, which is why it has such a high adoption rate among developers. It removes any liability from authors and does not explicitly contain a patent grant.It gives maximum freedom to the user as it has very less constraints. 
